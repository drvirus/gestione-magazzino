<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style/style.css">
<title>Registrazione Nuovo Cliente</title>
</head>
<body>
<div id="menu">

		<ul class="menu">
			<li class="menu"> <a class="menu" href="/HibernateAndStruts"> Homepage </a>  </li>
			<li class="menu"> <a class="menu" href="cliente.jsp"> Gestione Cliente </a>  </li>
			<li class="menu"> <a class="menu" href="magazzino.jsp"> Gestione Magazzino </a>  </li>
			<li class="menu"> <a class="menu" href="stats.jsp"> Statistiche </a>  </li>
			<li class="menu"> <a class="menu" href="regadmin.jsp"> Registra Admin </a>  </li>
		</ul>

	</div>

<br/> <br/>
<h2> Registrazione Nuovo Cliente </h2>

	<div id="menuPrincipale">

		<ul class="menu">
			<li class="menuHorizontal"> <a class="menu" href="clienteRegistra.jsp"> Registra Cliente</a> </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteCerca.jsp"> Cerca Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteElimina.jsp"> Elimina Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteModifica.jsp"> Modifica Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteRicercaID.jsp"> Ricerca ID </a>  </li>
		</ul>

	</div>
	
	<s:if test="hasActionErrors()">
   		<div class="errors">
      		<s:actionerror/>
   		</div>
	</s:if>

	
	<div id="lateral">
		<form action="RegistraClienteAction" method="post">
			<table cellpadding="5" cellspacing="5">
				<tr> 
					<td>Nome</td> <td> <input type="text" name="nome"/> </td>
				</tr>
				<tr>
					<td>Cognome</td> <td> <input type="text" name="cognome"/> </td>
				</tr>
				
				<tr>
					<td>Città</td> <td> <input type="text" name="citta"/> </td>
				</tr>
				
				<tr>
					<td>Indirizzo</td> <td> <input type="text" name="indirizzo"/> </td>
				</tr>
				
				<tr>
					<td>Numero Civico</td> <td> <input type="text" name="numeroCivico"/> </td>
				</tr>
				<tr>
					<td>e-mail</td> <td> <input type="text" name="mail"/> </td>
				</tr>
				<tr>
					<td>Data di Nascita</td> <td> <input type="date" name="dataNasc"/> </td>
				</tr>

				<tr>
					<td></td>
					<td align="right"><input type="submit" value="Registra Utente"> </td>
				</tr>
				
			</table>
		</form>
	</div>
	
	

</body>
</html>