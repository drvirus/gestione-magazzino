<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<link rel="stylesheet" href="style/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ricerca Cliente ID</title>
</head>
<body>
<h1>Gestione Magazzino 1.0</h1>
<div id="menu">

		<ul class="menu">
			<li class="menu"> <a class="menu" href="/HibernateAndStruts"> Homepage </a>  </li>
			<li class="menu"> <a class="menu" href="cliente.jsp"> Gestione Cliente </a>  </li>
			<li class="menu"> <a class="menu" href="magazzino.jsp"> Gestione Magazzino </a>  </li>
			<li class="menu"> <a class="menu" href="stats.jsp"> Statistiche </a>  </li>
			<li class="menu"> <a class="menu" href="regadmin.jsp"> Registra Admin </a>  </li>
		</ul>

	</div>

<br/> <br/>
<h2> Ricerca Cliente ID</h2>

<div id="menuPrincipale">

		<ul class="menu">
			<li class="menuHorizontal"> <a class="menu" href="clienteRegistra.jsp"> Registra Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteCerca.jsp"> Cerca Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteElimina.jsp"> Elimina Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteModifica.jsp"> Modifica Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteRicercaID.jsp"> Ricerca ID </a>  </li>
		</ul>

	</div>

	<s:if test="hasActionErrors()">
   		<div class="errors">
      		<s:actionerror/>
   		</div>
	</s:if>
	
	
	<div id="lateral">
		<form action="RicercaClienteIDAction" method="post">
			<table>
				<tr> 
					<td>E-Mail</td> <td> <input type="text" name="email"/> </td>
				</tr>
				<tr>
				<td></td>
					<td><input type="submit" value="Cerca Cliente">  </td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>