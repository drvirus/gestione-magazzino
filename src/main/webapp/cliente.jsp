<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> 
<link rel="stylesheet" href="style/style.css">

<title>Gestione Cliente</title>
</head>
<body>

<h1>Gestione Magazzino 1.0</h1>
	<div id="menu">

		<ul class="menu">
			<li class="menu"> <a class="menu" href="/HibernateAndStruts"> Homepage </a>  </li>
			<li class="menu"> <a class="menu" href="cliente.jsp"> Gestione Cliente </a>  </li>
			<li class="menu"> <a class="menu" href="magazzino.jsp"> Gestione Magazzino </a>  </li>
			<li class="menu"> <a class="menu" href="stats.jsp"> Statistiche </a>  </li>
			<li class="menu"> <a class="menu" href="regadmin.jsp"> Registra Admin </a>  </li>
		</ul>

	</div>

<br/> <br/>
<h2> Gestione Cliente </h2>

<div id="menu">

		<ul class="menu">
			<li class="menuHorizontal"> <a class="menu" href="clienteRegistra.jsp"> Registra Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteCerca.jsp"> Cerca Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteElimina.jsp"> Elimina Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteModifica.jsp"> Modifica Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteRicercaID.jsp"> Ricerca ID </a>  </li>
		</ul>

	</div>
	
	

</body>
</html>