<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style/style.css">
<title>Cliente Trovato</title>
</head>
<body>

<div id="menu">

		<ul class="menu">
			<li class="menu"> <a class="menu" href="/HibernateAndStruts"> Homepage </a>  </li>
			<li class="menu"> <a class="menu" href="cliente.jsp"> Gestione Cliente </a>  </li>
			<li class="menu"> <a class="menu" href="magazzino.jsp"> Gestione Magazzino </a>  </li>
			<li class="menu"> <a class="menu" href="stats.jsp"> Statistiche </a>  </li>
			<li class="menu"> <a class="menu" href="regadmin.jsp"> Registra Admin </a>  </li>
		</ul>

	</div>

<br/> <br/>
<h2> Ricerca Cliente Anagrafica </h2>

<div id="menuPrincipale">

		<ul class="menu">
			<li class="menuHorizontal"> <a class="menu" href="clienteRegistra.jsp"> Registra Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteCerca.jsp"> Cerca Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteElimina.jsp"> Elimina Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteModifica.jsp"> Modifica Cliente </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="clienteRicercaID.jsp"> Ricerca ID </a>  </li>
		</ul>

	</div>
	
	<div id="lateral">
	<h3 align="center">Clienti Trovati</h3>
		<table cellspacing="11" cellpadding="4" border="1">
			<s:iterator value="result">
				<tr> <td> <b> Nome</b> </td>  <td> <s:property value="nome"/></td> </tr>
				<tr><td> <b> Cognome </b></td> <td> <s:property value="cognome"/></td></tr>
				<tr><td><b> E-mail</b></td> <td>  <s:property value="email"/></td></tr>
				<tr><td> <b>Città</b> </td> <td> <s:property value="citta"/></td> </tr>
				<tr><td> <b>Indirizzo</b> </td> <td> <s:property value="indirizzo"/> </td></tr>
				<tr><td><b>Num. Civico</b> </td><td> <s:property value="numCivico"/> </td></tr>
				<tr><td> <b>Data di Nascita</b> </td> <td> <s:property value="dataNasc"/> </td></tr>
				<tr><td align="center"> --------- </td><td align="center"> --------- </td></tr>
			</s:iterator>
		</table>
		
	
	
	</div>

</body>
</html>