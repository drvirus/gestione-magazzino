<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style/style.css">

<title>Gestione Magazzino</title>
</head>
<body>
<h1>Gestione Magazzino 1.0</h1>

<div id="menu">

		<ul class="menu">
			<li class="menu"> <a class="menu" href="/HibernateAndStruts"> Homepage </a>  </li>
			<li class="menu"> <a class="menu" href="cliente.jsp"> Gestione Cliente </a>  </li>
			<li class="menu"> <a class="menu" href="magazzino.jsp"> Gestione Magazzino </a>  </li>
			<li class="menu"> <a class="menu" href="stats.jsp"> Statistiche </a>  </li>
			<li class="menu"> <a class="menu" href="regadmin.jsp"> Registra Admin </a>  </li>
		</ul>

	</div>
	<br/> <br/>
<h2> Gestione Magazzino </h2>

<div id="menu">

		<ul class="menu">
			<li class="menuHorizontal"> <a class="menu" href="inserisciprodotti.jsp"> Inserisci Prodotti </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="magazzinoOrdina.jsp"> Ordina </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="magazzinoApri.jsp"> Apri Magazzino </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="magazzinoElimina.jsp"> Annulla Ordine </a>  </li>
			<li class="menuHorizontal"> <a class="menu" href="calcolafattura.jsp"> Calcola Fattura </a>  </li>
		</ul>

	</div>

</body>
</html>