package com.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.model.Cliente;
import com.opensymphony.xwork2.ActionSupport;

import controller.RicercaIDController;

public class RicercaClienteIDAction extends ActionSupport{

	private static final long serialVersionUID = -5910209185881248165L;
	private String nome;
	private String cognome;
	private String citta;
	private String email;
	private String indirizzo;
	private int numeroCivico;
	private Date dataNasc;
	private	List<Cliente> result = new ArrayList<Cliente>();
	
	
	
	public String execute(){
		
		Cliente cli = new Cliente();
		cli.setEmail(email);
		
		RicercaIDController controllerID= new RicercaIDController();
		result=controllerID.cercaClienteID(cli);
		return SUCCESS;
	}

	public void validate(){
		
		if(email.equalsIgnoreCase("")){
			addActionError("Inserisci email");
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public int getNumeroCivico() {
		return numeroCivico;
	}

	public void setNumeroCivico(int numeroCivico) {
		this.numeroCivico = numeroCivico;
	}

	public Date getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}

	public List<Cliente> getResult() {
		return result;
	}

	public void setResult(List<Cliente> result) {
		this.result = result;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
