package com.action;

import java.util.Date;

import com.model.Cliente;
import com.opensymphony.xwork2.ActionSupport;

import controller.RegistraClienteCtl;

public class RegistraClienteAction extends ActionSupport{

	private static final long serialVersionUID = 3831571969070802553L;
	
	private String nome;
	private String cognome;
	private String citta;
	private String mail;
	private String indirizzo;
	private int numeroCivico;
	private Date dataNasc;
	
	
	public String execute() throws Exception{
		
		System.out.println(nome);
		
		System.err.println(cognome);
		Cliente cli = new Cliente(nome, cognome, citta, indirizzo, mail, dataNasc, numeroCivico);
		RegistraClienteCtl clientctl = new RegistraClienteCtl();
		clientctl.writeDB(cli);
		return SUCCESS;
	}
	
	public void validate(){
		if(nome.equals("")){
			addActionError("Inserisci il nome");
		}
		else if(cognome.equalsIgnoreCase("")){
			addActionError("Inserisci cognome");
		}
		else if(citta.equalsIgnoreCase("")){
			addActionError("Inserisci Città");
		}
		else if(indirizzo.equalsIgnoreCase("")){
			addActionError("Inserisci indirizzo");
		}
		else if(mail.equalsIgnoreCase("")){
			addActionError("Inserisci una mail valida");
		}
		else if(numeroCivico==0){
			addActionError("Inserisci un numero civico valido");
		}
		
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCognome() {
		return cognome;
	}


	public void setCognome(String cognome) {
		this.cognome = cognome;
	}


	public String getCitta() {
		return citta;
	}


	public void setCitta(String citta) {
		this.citta = citta;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getIndirizzo() {
		return indirizzo;
	}


	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}


	public int getNumeroCivico() {
		return numeroCivico;
	}


	public void setNumeroCivico(int numeroCivico) {
		this.numeroCivico = numeroCivico;
	}


	public Date getDataNasc() {
		return dataNasc;
	}


	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
