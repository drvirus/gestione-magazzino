package com.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.model.Cliente;
import com.opensymphony.xwork2.ActionSupport;

import controller.ModificaClienteController;

public class ModificaClienteAction extends ActionSupport {

	private static final long serialVersionUID = -6774085442265966989L;
	private String nome;
	private String cognome;
	private String citta;
	private String email;
	private String indirizzo;
	private int numeroCivico;
	private Date dataNasc;
	private int id_cliente;
	List<Cliente> result = new ArrayList<Cliente>();

	
	public String execute(){
		ModificaClienteController controllerMod= new ModificaClienteController();
		Cliente cli = controllerMod.VerificaCliente(id_cliente);
		if((cli)!=null){
			
			nome=cli.getNome();
			cognome=cli.getCognome();
			citta=cli.getCitta();
			email=cli.getEmail();
			numeroCivico=cli.getNumCivico();
			indirizzo=cli.getIndirizzo();
			dataNasc=cli.getDataNasc();
			id_cliente=cli.getId_cliente();
			return SUCCESS;
		}
		else{
			addActionError("Cliente non trovato");
		}
		
		return ERROR;
	}
	
	public String salvamodifiche(){
		
		
		//cli.setId_cliente(id_cliente);
		ModificaClienteController controllerMod= new ModificaClienteController();
		Cliente cli= controllerMod.VerificaCliente(id_cliente);
		cli.setCitta(citta);
		cli.setCognome(cognome);
		cli.setDataNasc(dataNasc);
		cli.setEmail(email);
		cli.setId_cliente(id_cliente);
		cli.setIndirizzo(indirizzo);
		cli.setNome(nome);
		cli.setNumCivico(numeroCivico);
		
		boolean result=controllerMod.modificaCliente(cli);
		if (! result)
			addActionError("Nessun Cliente Trovato effettuare nuovamente una ricerca");
		return SUCCESS;
	}
	
	public void validate(){
		
	}
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public int getNumeroCivico() {
		return numeroCivico;
	}
	public void setNumeroCivico(int numeroCivico) {
		this.numeroCivico = numeroCivico;
	}
	public Date getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public List<Cliente> getResult() {
		return result;
	}
	public void setResult(List<Cliente> result) {
		this.result = result;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
