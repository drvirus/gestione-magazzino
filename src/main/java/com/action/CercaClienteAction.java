package com.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.model.Cliente;
import com.opensymphony.xwork2.ActionSupport;

import controller.CercaAnagController;

public class CercaClienteAction extends ActionSupport {

	private static final long serialVersionUID = 369297268488381579L;
	private String nome;
	private String cognome;
	private String citta;
	private String mail;
	private String indirizzo;
	private int numeroCivico;
	private Date dataNasc;
	private	List<Cliente> result = new ArrayList<Cliente>();

	public List<Cliente> getResult() {
		return result;
	}


	public void setResult(List<Cliente> result) {
		this.result = result;
	}


	public String execute(){
		
		Cliente cli = new Cliente();
		cli.setNome(nome);
		cli.setCognome(cognome);
		cli.setEmail(mail);
		CercaAnagController anagSearch = new CercaAnagController();
		result=anagSearch.listaClienti(cli);
		for(int i=0;i<result.size();i++){
			String data= result.get(i).getDataNasc().toString();
			//data.substring(0, 6);
			data=data.substring(0,10);
			System.out.println("dopo substring: "+data);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date datafinal=null;
			
			try {
				datafinal = df.parse(data);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			result.get(i).setDataNasc(datafinal);
			System.out.println("dopo set: "+datafinal);
			
		}
		return SUCCESS;
	}
	
	
	public void validate(){
		if(nome.equals("")){
			addActionError("Inserisci il nome");
		}
		else if(cognome.equalsIgnoreCase("")){
			addActionError("Inserisci cognome");
		}
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public int getNumeroCivico() {
		return numeroCivico;
	}

	public void setNumeroCivico(int numeroCivico) {
		this.numeroCivico = numeroCivico;
	}

	public Date getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
	
	
}
