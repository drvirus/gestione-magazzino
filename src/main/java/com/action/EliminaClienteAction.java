package com.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.model.Cliente;
import com.opensymphony.xwork2.ActionSupport;

import controller.EliminaClienteController;

public class EliminaClienteAction extends ActionSupport{

	
	private static final long serialVersionUID = -4302101532457687093L;
	private String nome;
	private String cognome;
	private String citta;
	private String mail;
	private String indirizzo;
	private int numeroCivico;
	private Date dataNasc;
	private int id_cliente;
	List<Cliente> result = new ArrayList<Cliente>();
	
	public String execute(){
		
		Cliente cli = new Cliente();
		Cliente tmp=null;
		
		cli.setId_cliente(id_cliente);
		EliminaClienteController controller = new EliminaClienteController();
		tmp= controller.eliminaCliente(cli);
		if(tmp!=null){
			addActionError("Cliente Eliminato");
		}
		else if(tmp==null){
			addActionError("Cliente Non Trovato");
		}
		return SUCCESS;
	}
	
	public void validate(){
		if(id_cliente==0){
			addActionError("Inserisci un id Valido");
		}
	}
	
	public List<Cliente> getResult() {
		return result;
	}

	public void setResult(List<Cliente> result) {
		this.result = result;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public int getNumeroCivico() {
		return numeroCivico;
	}
	public void setNumeroCivico(int numeroCivico) {
		this.numeroCivico = numeroCivico;
	}
	public Date getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
