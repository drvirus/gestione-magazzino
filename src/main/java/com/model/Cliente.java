package com.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table (name = "cliente")
public class Cliente {
	
	@Id @GeneratedValue
	@Column (name = "id_cliente")
	private int id_cliente;
	
	@Column (name = "nome")
	private String nome;
	
	@Column (name = "cognome")
	private String cognome;
	
	@Column (name = "citta")
	private String citta;
	
	@Column (name = "indirizzo")
	private String indirizzo;
	
	@Column (name = "numCivico")
	private int numCivico;
	
	@Column (name = "email")
	private String email;
	
	@Column (name = "dataNasc")
	private Date dataNasc;
	/*
	@OneToMany (mappedBy="Ordine",cascade=CascadeType.PERSIST)
	private List<Ordine> listaordini = new ArrayList<Ordine>();
	*/
	public Cliente(){
		
	}
	
	public Cliente(String nome, String cognome, String citta, String indirizzo, String email, Date dataNasc, int numCivico) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.citta = citta;
		this.indirizzo = indirizzo;
		this.email = email;
		this.dataNasc = dataNasc;
		this.numCivico=numCivico;
	}

	
	public int getNumCivico() {
		return numCivico;
	}

	


	public int getId_cliente() {
		return id_cliente;
	}


	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}


	public void setNumCivico(int numCivico) {
		this.numCivico = numCivico;
	}



	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCognome() {
		return cognome;
	}


	public void setCognome(String cognome) {
		this.cognome = cognome;
	}


	public String getCitta() {
		return citta;
	}


	public void setCitta(String citta) {
		this.citta = citta;
	}


	public String getIndirizzo() {
		return indirizzo;
	}


	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Date getDataNasc() {
		return dataNasc;
	}


	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}

	@Override
	public String toString() {
		return "Cliente [id_Cliente=" + id_cliente + ", nome=" + nome + ", cognome=" + cognome + ", citta=" + citta
				+ ", indirizzo=" + indirizzo + ", numCivico=" + numCivico + ", email=" + email + ", dataNasc="
				+ dataNasc + "]";
	}
}
