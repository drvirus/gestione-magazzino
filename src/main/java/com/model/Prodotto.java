package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

public class Prodotto {
	
	@Id @GeneratedValue
	@Column (name = "id_prodotto")
	private int id_prodotto;
	
	@Column (name = "nome")
	private String nome;
	
	@Column (name = "quantita")
	private int quantita;
	
	@Column (name = "importo")
	private Double importo;

	/*@OneToMany(mappedBy="prodotto",cascade=CascadeType.PERSIST)
	private List<Ordine> listaordini = new ArrayList<Ordine>();
*/
	public Prodotto(int id_prodotto, String nome, int quantita, Double importo, List<Ordine> listaordini) {
		super();
		this.id_prodotto = id_prodotto;
		this.nome = nome;
		this.quantita = quantita;
		this.importo = importo;
		//this.listaordini = listaordini;
	}
	
	
	public Prodotto(){
		super();
	}


	public int getId_prodotto() {
		return id_prodotto;
	}


	public void setId_prodotto(int id_prodotto) {
		this.id_prodotto = id_prodotto;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public int getQuantita() {
		return quantita;
	}


	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}


	public Double getImporto() {
		return importo;
	}


	public void setImporto(Double importo) {
		this.importo = importo;
	}


	/*public List<Ordine> getListaordini() {
		return listaordini;
	}


	public void setListaordini(List<Ordine> listaordini) {
		this.listaordini = listaordini;
	}*/


	@Override
	public String toString() {
		return "Prodotto [id_prodotto=" + id_prodotto + ", nome=" + nome + ", quantita=" + quantita + ", importo="
				+ importo +"]";
	}
	
}
