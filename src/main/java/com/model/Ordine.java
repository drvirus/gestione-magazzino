package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

public class Ordine {
	
	
	@Id @GeneratedValue
	@Column (name = "id_ordine")
	private int id_ordine;
	
	@Column (name = "id_prodotto")
	private int id_prodotto;
	
	@Column (name = "id_cliente")
	private int id_cliente;
	
	@Column (name = "dataOrdine")
	private Date dataOrdine;
	
	@Column (name = "dataEvasione")
	private Date dataEvasione;
	

/*	@ManyToOne
	private Cliente cliente;
	
	@ManyToOne
	private Prodotto prodotto;
	*/
	
	public Ordine(){
		super();
	}
	
	

	public Ordine(int id_ordine, int id_prodotto, int id_cliente, Date dataOrdine, Date dataEvasione, Cliente cliente,
			Prodotto prodotto) {
		super();
		this.id_ordine = id_ordine;
		this.id_prodotto = id_prodotto;
		this.id_cliente = id_cliente;
		this.dataOrdine = dataOrdine;
		this.dataEvasione = dataEvasione;
		/*this.cliente = cliente;
		this.prodotto = prodotto;
	*/}

	public int getId_ordine() {
		return id_ordine;
	}

	public void setId_ordine(int id_ordine) {
		this.id_ordine = id_ordine;
	}

	public int getId_prodotto() {
		return id_prodotto;
	}

	public void setId_prodotto(int id_prodotto) {
		this.id_prodotto = id_prodotto;
	}

	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public Date getDataOrdine() {
		return dataOrdine;
	}

	public void setDataOrdine(Date dataOrdine) {
		this.dataOrdine = dataOrdine;
	}

	public Date getDataEvasione() {
		return dataEvasione;
	}

	public void setDataEvasione(Date dataEvasione) {
		this.dataEvasione = dataEvasione;
	}

/*	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Prodotto getProdotto() {
		return prodotto;
	}

	public void setProdotto(Prodotto prodotto) {
		this.prodotto = prodotto;
	}
*/
	@Override
	public String toString() {
		return "Ordine [id_ordine=" + id_ordine + ", id_prodotto=" + id_prodotto + ", id_cliente=" + id_cliente
				+ ", dataOrdine=" + dataOrdine + ", dataEvasione=" + dataEvasione 
				+ ", prodotto=" +"]";
	}
	
	
}
