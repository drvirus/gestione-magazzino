package controller;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.model.Cliente;

public class RegistraClienteCtl {
	
	@SuppressWarnings("deprecation")
	public Cliente writeDB(Cliente cli){
		
		SessionFactory factory;
		Session session = null;
		Transaction tx=null;
		
		try{
			factory= new org.hibernate.cfg.Configuration().configure().buildSessionFactory();
			session=factory.openSession();
			tx=session.beginTransaction();
			
			session.save(cli);
			tx.commit();
			
		}
		catch(HibernateException e){
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		
		
		return cli;
	}

}
