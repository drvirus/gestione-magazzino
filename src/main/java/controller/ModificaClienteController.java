package controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.model.Cliente;

public class ModificaClienteController {

	
	public Cliente VerificaCliente(int id){
		List<Cliente> result = null;
		
		Session session =null;
		SessionFactory factory=null;
		Transaction tx=null;
		try{
			factory= new Configuration().configure().buildSessionFactory();
			session=factory.openSession();
			//tx=session.beginTransaction();
			Criteria cr = session.createCriteria(Cliente.class);
			cr.add(Restrictions.eq("id_cliente", id));
			result=cr.list();
			
			int size=result.size();
			if(size<=0){
				return null;
			}
			//tx.commit();
		}
		catch(HibernateException e){
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return result.get(0);
	}
	
	public boolean modificaCliente(Cliente cli){
		boolean result = true;
		
		SessionFactory factory=null;
		Session session =null;
		Transaction tx=null;
		
		try{
			factory= new Configuration().configure().buildSessionFactory();
			session= factory.openSession();
			tx=session.beginTransaction();
			session.update(cli);
			tx.commit();
		}
		catch(HibernateException e){
			if(tx!=null){	
				tx.rollback();
			}
			e.printStackTrace();
			result = false;
		}
		finally{
			session.close();
		}
		
		return result;
	}
}
