package controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.model.Cliente;

public class EliminaClienteController {
	
	public Cliente eliminaCliente(Cliente cli){
		List<Cliente> result = new ArrayList<Cliente>();
		
		Session session =null;
		SessionFactory factory=null;
		Transaction tx=null;
		try{
			factory= new Configuration().configure().buildSessionFactory();
			session=factory.openSession();
			tx=session.beginTransaction();
			
			Criteria cr = session.createCriteria(Cliente.class);
			cr.add(Restrictions.eq("id_cliente", cli.getId_cliente()));
			result=cr.list();
			int size=result.size();
			if(size>0){
				for(int i=0;i<size;i++){
					session.delete(result.get(i));
					System.out.println("cliente eliminato: "+i);
				}
			}
			else if(size==0){
				cli=null;
				return cli;
			}
			System.out.println("elimina list size: "+result.size());
			
			tx.commit();
		}
		catch(HibernateException e){
			if(tx!=null){
				tx.rollback();
			}
		}
		finally{
			session.close();
		}
		
		return cli;
	}

}
