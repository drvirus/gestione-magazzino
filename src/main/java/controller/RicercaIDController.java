package controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.model.Cliente;

public class RicercaIDController {
	
	public List<Cliente> cercaClienteID (Cliente cli){
		
		List<Cliente> result= new ArrayList<Cliente>();
		SessionFactory factory=null;
		Session session =null;
		Transaction tx=null;
		
		try{
			factory = new Configuration().configure().buildSessionFactory();
			session=factory.openSession();
			tx=session.beginTransaction();
			
			Criteria cr = session.createCriteria(Cliente.class);
			cr.add(Restrictions.eq("email", cli.getEmail()));
			result=cr.list();
			System.out.println("size lista id: "+result.size());
			tx.commit();
		}
		catch(HibernateException e){
			if(tx!=null){
				tx.rollback();
			}
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		
		return result;
	}

}
