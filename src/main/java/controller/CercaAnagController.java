package controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

import com.model.Cliente;

public class CercaAnagController {
	public List<Cliente> listaClienti(Cliente cli){
		List<Cliente> result = new ArrayList<Cliente>();
		
		SessionFactory factory = null;
		Session session = null;
		Transaction tx = null;
		
		
		try{
			factory= new Configuration().configure().buildSessionFactory();
			session=factory.openSession();
			tx=session.beginTransaction();
			Criteria cr = session.createCriteria(Cliente.class);
			/*cr.add(
					Restrictions.and(
							Restrictions.eq("nome", cli.getNome()), 
							Restrictions.eq("cognome", cli.getCognome())
							)
					);*/
			
			Criterion sname= Restrictions.eq("nome", cli.getNome());
			Criterion scognome= Restrictions.eq("cognome", cli.getCognome());
			LogicalExpression logicalAnd = Restrictions.and(sname, scognome);
			cr.add(logicalAnd);
			result=cr.list();
			tx.commit();
		}
		catch(HibernateException e){
			if(tx!=null){
				tx.rollback();
			}
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		
		
		return result;
	}

}
